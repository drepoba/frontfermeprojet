import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { LayoutComponent } from "../layout/layout.component";
import { Lapin } from "../model/entities";

@Injectable({
    providedIn: 'root'
  })
export class LapinService{
    prefixeUrl: string = "api/v1/lapin";
    public URL_FILE:string="http://localhost/img/";

    constructor(private http: HttpClient, private router: Router) {
    }

    public getAllLapinsList() {
        return this.http.get(LayoutComponent.RESOURCE_SERVER_URL + "/" + this.prefixeUrl + "/list");
      }
    
      public findLapinById(code: string) {
        return this.http.get(LayoutComponent.RESOURCE_SERVER_URL + "/" + this.prefixeUrl + "/find/" + code);
      }
    
      public saveLapin(lapin: Lapin) {
        let data = {
            "nom":lapin.nom,
            "sexe":lapin.sexe,
            "dateAjout":lapin.dateAjout,
            "statut":lapin.statut,
            "fileName":lapin.fileName

        };
        return this.http.post(LayoutComponent.RESOURCE_SERVER_URL + "/" + this.prefixeUrl + "/save", data);
      }

      public uploadFile(formData: FormData) {
        return this.http.post(LayoutComponent.RESOURCE_SERVER_URL + "/" + this.prefixeUrl + "/upload", formData);
      }
    
      public updateLapin(id:string,lapin: Lapin) {
        let data = {
            "nom":lapin.nom,
            "sexe":lapin.sexe,
            "dateAjout":lapin.dateAjout,
            "statut":lapin.statut,
            "fileName":lapin.fileName
        };
        return this.http.put(LayoutComponent.RESOURCE_SERVER_URL + "/" + this.prefixeUrl + "/update/"+id, data);
      }
    
      public deleteLapin(lapinId: string) {
        return this.http.delete(LayoutComponent.RESOURCE_SERVER_URL + "/" + this.prefixeUrl + "/delete/" + lapinId);
      }
    
}