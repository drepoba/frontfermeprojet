import { Component } from '@angular/core';
import { Lapin } from '../model/entities';
import { LapinService } from '../services/lapinService';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
declare var $: any;
import Swal from 'sweetalert2';




@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent {
  showModal = false;
  currentPage: number = 1;
  lapin:Lapin=new Lapin;
  selectedFile: File | null=null;
  imageUrl: string | null=null;
  lapinList:any=[];
  

constructor(public lapinService:LapinService){
  this.getAllList();
}

  onFileSelected(event: any) {
    this.selectedFile = event.target.files[0] as File;
    this.imageUrl = URL.createObjectURL(this.selectedFile);
    this.lapin.fileName=this.selectedFile?.name;
  }
 
  openModal(param:string) {
    if(param=="save")
     $('#myModal').modal('show');
    else 
    $('#myModalUpdate').modal('show');
  }

  closeModal() {
    $('#myModal').modal('hide');
  }
  closeModalUpdate(){
    $('#myModalUpdate').modal('hide'); 
  }

  uploadFile(){
    if (!this.selectedFile) {
      return; // Aucun fichier sélectionné, ne rien faire
    }

    const formData = new FormData();
    formData.append('file', this.selectedFile, this.selectedFile.name);
      console.log(this.selectedFile.name);
      this.lapinService.uploadFile(formData)
      .subscribe({
        next: (data: any) => {
          // Traitement des données reçues
          console.log('Fichier envoyé avec succès:', data);
          // Afficher l'URL de l'image après l'envoi réussi (si le serveur renvoie l'URL)
          this.imageUrl = data.imageUrl;
        },
        error: (erreur: any) => {
          // Gestion des erreurs
          console.error('Erreur lors de l\'envoi du fichier:', erreur);
        },
        complete: () => {
          // Actions à exécuter après la complétion de l'observable (facultatif)
        }
      });
  }

  getAllList(){
    this.lapinService.getAllLapinsList()
    .subscribe({
      next:(data:any)=>{
        this.lapinList=data;
        this.imageUrl="";
      },
      error:(error)=>{
        console.log(error);
      }
    })
  }

  saveLapin(){
    this.lapin.fileName=this.selectedFile?.name
    console.log(this.lapin);
    this.lapinService.saveLapin(this.lapin)
    .subscribe({
      next:(data:any)=>{
        this.lapin=new Lapin();
        this.uploadFile();
        this.getAllList();
        this.closeModal();
        this.messageSucces();
      },
      error:(error:any)=>{
        console.log(error);
      }
    })
  }

  findOne(id:string){
    this.lapinService.findLapinById(id)
    .subscribe({
      next:(data:any)=>{
        this.lapin=data;
        this.imageUrl=this.lapinService.URL_FILE + this.lapin.fileName;
        this.openModal("update");
      }
    })
  }

  deleteBYId(id:string){
    this.lapinService.deleteLapin(id)
    .subscribe({
      next:(data:any)=>{
          this.messageSucces();
          this.getAllList();
      },
      error:(error:any)=>{
       console.log(error);
      }
    })
  }

  update(id:string,lapin:Lapin){
    console.log(lapin);
    this.lapinService.updateLapin(id,lapin)
    .subscribe({
      next:(data:any)=>{
        this.getAllList();
        this.closeModalUpdate();
        this.messageSucces();
      }
    })
  }

  messageSucces(){
    Swal.fire({
      title: 'Action effectuée',
      text: 'Action effectuée avec succès!',
      icon: 'success',
      timer: 2000, // Durée en millisecondes avant que l'alerte ne disparaisse (ici, 2 secondes)
      showConfirmButton: false // Ne pas afficher le bouton "OK" pour fermer l'alerte manuellement
    });
  }

  confirmDelete(id:string){
    Swal.fire({
      title: 'Êtes-vous sûr?',
      text: 'Voulez-vous vraiment supprimer cet element?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Oui, Supprimer!',
      cancelButtonText: 'Annuler'
    }).then((result) => {
      if (result.isConfirmed) {
        this.deleteBYId(id);
        console.log('suppression effectuée!');
      }
    });
  }

  confirmSave(){
    Swal.fire({
      title: 'Êtes-vous sûr?',
      text: 'Voulez-vous vraiment ajouter cet element?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Oui, ajouter!',
      cancelButtonText: 'Annuler'
    }).then((result) => {
      if (result.isConfirmed) {
        this.saveLapin();
        console.log('ajout effectué!');
      }
    });
  }

  confirmUpdate(id:string,updateLapin:Lapin){
    Swal.fire({
      title: 'Êtes-vous sûr?',
      text: 'Voulez-vous vraiment modifier cet element?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Oui, Modifier!',
      cancelButtonText: 'Annuler'
    }).then((result) => {
      if (result.isConfirmed) {
        this.update(id,updateLapin);
        console.log('modification effectuée!');
      }
    });
  }
}
